# Rock - Paper - Scissors

<p>Very simple, console based, java rock, paper, scissors game.

## License

<p> This software is licensed under the [AGPL3](https://www.gnu.org/licenses/agpl-3.0.html) license.
<p> ![GPL](https://www.gnu.org/graphics/slickgnu.tiny.png "GNU logo")
