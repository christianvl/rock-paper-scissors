//This is a game program to play rock - paper - scisors
//by Christian van Langendonck
//20191101
//GPL3

import java.util.Scanner;
import java.util.Random;

public class rps {

public static void main (String[] args){

//variables
String playerChoice, computerChoice;
int computerChoiceRandom, playerScore, computerScore, turns;
playerScore = 0;
computerScore = 0;

//start scanner
Scanner input = new Scanner(System.in);

//set the number of turns
for (turns = 0; playerScore < 2 && computerScore < 2; turns++) {

	//start user input
	System.out.println("Type in rock, paper or scissors");
	playerChoice = input.next();
	while (!playerChoice.equals("rock") && !playerChoice.equals("paper") && !playerChoice.equals("scissors")){
	System.out.println("You must type rock, paper or scissors");
	playerChoice = input.next();
		}

	//computer input
	Random rand = new Random();
	computerChoiceRandom = rand.nextInt(2);
	if (computerChoiceRandom == 0) {
		computerChoice = "rock";
		}
	else if (computerChoiceRandom == 1) {
		computerChoice = "paper";
		}
	else {
		computerChoice = "scissors";
		}
	System.out.println("Computer plays " + computerChoice);

	//compare inputs
	if (playerChoice.equals(computerChoice)) {
		System.out.println("It's a draw!");
		}
	else if (playerChoice.equals("rock") && computerChoice.equals("paper")) {
		System.out.println("Computer wins!");
		computerScore = ++computerScore;
	}
	else if (playerChoice.equals("paper") && computerChoice.equals("scissors")) {
		System.out.println("Computer wins!");
		computerScore = ++computerScore;
	}
	else if (playerChoice.equals("scissors") && computerChoice.equals("rock")) {
		System.out.println("Computer wins!");
		computerScore = ++computerScore;
		}
	else {
		System.out.println("Player wins!");
		playerScore = ++playerScore;
		}

	//Score
	System.out.println("Player [ " + playerScore + " ] vs. [ " + computerScore + " ] Computer");


}//end of loop for turns

	//final results
	if (playerScore > computerScore) {
		System.out.println("Congratulations! You're the winner!");
		}
	else if (playerScore < computerScore) {
		System.out.println("You're the looser! Better luck next time!");
		}
	else {
		System.out.println("Oh! It's a draw!");
	}
System.out.println("You've played " + turns + " turns!");
}//end of main

}//end of class
